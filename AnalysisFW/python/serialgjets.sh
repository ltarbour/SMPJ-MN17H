#!/bin/bash

# 1: Choose 16/17/18 (for MC: 16e/16l and 18e/18l)
# 2: Era or MC type (only 'py' supported) 

#python gjetsConfig.py '16e' 'py'
#python gjetsConfig.py '16l' 'py'

#python gjetsConfig.py '17' 'py'

python gjetsConfig.py '18' 'py'
